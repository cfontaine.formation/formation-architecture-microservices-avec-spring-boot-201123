package fr.dawan.springboot.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.dto.ArticleDto;
import fr.dawan.springboot.services.ArticleService;

@RestController
@RequestMapping("/api/v1/articles")
public class ArticleController extends GenericController<ArticleDto, Long> {

    public ArticleController(ArticleService service) {
        super(service);
    }
}
