package fr.dawan.springboot.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
    
    @Value("${message.hello}")
    private String message;

    @GetMapping(value="/hello")
    public String helloworld() {
        return message;
    }
    
}
