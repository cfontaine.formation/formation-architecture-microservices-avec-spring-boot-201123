package fr.dawan.springboot.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.services.MarqueService;

@RestController
 @RequestMapping("/api/v1/marques")
public class MarqueController {
   
    @Autowired
    private MarqueService service;
    
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE) //value=""
    public List<MarqueDto> getAllMarque() {
        return service.getAllMarques(Pageable.unpaged());
    }
    
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE,params= {"size","page"}) 
    public List<MarqueDto> getAllMarquePage(Pageable page){
        return service.getAllMarques(page);
    }
    
    @GetMapping(value="/{id:[0-9]+}",produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MarqueDto> getByIdMarque(@PathVariable long id) {
        try {
            return ResponseEntity.ok(service.getMarqueById(id));
        } catch (Exception e) {
           return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value="/{nom:[a-zA-Z]+}",produces=MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> getMarquebyName(@PathVariable String nom) {
        return service.getMarqueByNom(nom+"%");
    }
    
    @DeleteMapping(value="/{id}",produces=MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteMarque(@PathVariable long id) {
        if(service.deleteMarque(id)) {
            return new ResponseEntity<>("la marque id="+id+ "est supprimée", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("la marque id="+id+ "n'existe pas", HttpStatus.NOT_FOUND);
        }
    }
    
    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE) //produces="application/json"
    public MarqueDto create(@RequestBody MarqueDto marque) {
        return service.saveOrUpdate(marque);
    }
    
    @PutMapping(value="/{id}",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto update(@PathVariable long id, @RequestBody MarqueDto marque) {
        MarqueDto md=service.getMarqueById(id);
        if(md!=null) {
            md.setNom(marque.getNom());
            md.setDateCreation(marque.getDateCreation());
        }
        return service.saveOrUpdate(md);
    }
    
    @GetMapping("/ioexception")
    public void genIOException() throws IOException{
        throw new IOException("Erreur E/S");
    }
    
    @GetMapping("/sqlexception")
    public void genSQLException() throws SQLException{
        throw new SQLException("Erreur SQL");
    }
    
    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> handlerIoException(IOException e){
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
