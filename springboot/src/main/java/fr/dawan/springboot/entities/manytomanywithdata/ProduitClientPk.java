package fr.dawan.springboot.entities.manytomanywithdata;

import java.io.Serializable;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode

@Embeddable
public class ProduitClientPk implements Serializable {

    private static final long serialVersionUID = 1L;

    private long produitId;

    private long clientId;
}
