package fr.dawan.springboot.entities.relation;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "maires")
public class Maire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    private String prenom;

    private String nom;

    // Relation @OneToOne bidirectionnelle
    // C'est la relation retour Maire -> Ville
    // pour la réaliser, on place sur la variable d'instance de type Ville
    // on utilise l'annotation @OneToOne avec l'attribut mappedBy qui a pour valeur
    // le nom de variable d'instance de l'autre coté de la relation -> ici maire
    // dans ce cas, on peut connaitre le maire à partir de la ville et la ville
    // depuis le maire
    @OneToOne(mappedBy = "maire")
    @Exclude
    private Ville ville;
}
