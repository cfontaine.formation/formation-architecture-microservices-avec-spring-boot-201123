package fr.dawan.springboot.entities.relation;

public enum Emballage {
    CARTON, PLASTIQUE, PAPIER, SANS
}
