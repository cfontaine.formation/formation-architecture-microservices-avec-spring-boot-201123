package fr.dawan.springboot.entities.relation;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="villes")
public class Ville implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    private String nom;

    @OneToOne // => Relation 1,1
    // ici, Une ville n'a que d'une seul ville et un maire n'est maire que d'une seule ville

    // Relation @OneToOne Unidirectionnel
    // on a uniquement une relation Ville -> Maire
    // et pas de relation Maire -> Ville, on n'a pas accés à la ville depuis Maire
    
    @Exclude    // si on utilise @ToString, il faut ajouter l'annotation @Exclude pour les attribut qui sont des entitées. 
                // pour qu'ils ne soient pas ajouté dans la méthode toString -> cycles 

    private Maire maire;
}
