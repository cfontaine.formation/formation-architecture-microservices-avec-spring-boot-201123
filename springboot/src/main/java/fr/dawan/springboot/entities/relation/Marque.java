package fr.dawan.springboot.entities.relation;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;
@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="marques")
@NamedEntityGraph(name="marque-article-graph",
    attributeNodes= {@NamedAttributeNode("articles")}
        )

public class Marque extends AbstractAuditing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(nullable = false,length = 50)
    private String nom;
    
    @Column(name="date_creation")
    private LocalDate dateCreation;
    
    @OneToMany(mappedBy = "marque",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    @Exclude
    private List<Article> articles=new ArrayList<>();
    
    // Relation unidirectionnelle avec un @OneToMany (à éviter)
//    @OneToMany
    // Il faut ajouter @JoinColumn, sinon une table de jointure va être créer
//    @JoinColumn(name="marque_id")
//    @Exclude
//    private List<Article> articles=new ArrayList<>();
}
