package fr.dawan.springboot;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.services.ArticleService;
import fr.dawan.springboot.services.MarqueService;

//@Component
@Order(2)
public class ServiceRunner implements CommandLineRunner {
    
    @Autowired
    private MarqueService service;

    @Autowired
    private ArticleService articleService;
    
    @Override
    public void run(String... args) throws Exception {
       System.out.println("Service Runner");
       
       service.getAllMarques(Pageable.unpaged()).forEach(System.out::println);
       System.out.println("------------------------");
       MarqueDto m=new MarqueDto("Marque I",LocalDate.of(2006,8,10));
       MarqueDto mr=service.saveOrUpdate(m);
       System.out.println(mr);
       mr.setNom("Marque Iu");
       System.out.println("------------------------");
       mr=service.saveOrUpdate(mr);
       System.out.println(service.getMarqueById(mr.getId()));
       System.out.println("------------------------");
       service.deleteMarque(mr.getId());
       System.out.println("------------------------");
       service.getAllMarques(Pageable.unpaged()).forEach(System.out::println);
       System.out.println("------------------------");
       
       articleService.getAll(Pageable.unpaged()).forEach(System.out::println);
       }

}
