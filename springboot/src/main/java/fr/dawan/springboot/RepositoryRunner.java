package fr.dawan.springboot;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import fr.dawan.springboot.entities.relation.Article;
import fr.dawan.springboot.entities.relation.Emballage;
import fr.dawan.springboot.entities.relation.Marque;
import fr.dawan.springboot.repositories.ArticleRepository;
import fr.dawan.springboot.repositories.MarqueCustomRepository;
import fr.dawan.springboot.repositories.MarqueRepository;

//@Component
@Order(1)
public class RepositoryRunner implements CommandLineRunner {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private MarqueRepository marqueRepository;
    
    @Autowired
    private MarqueCustomRepository marqueCustomRepository;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Repository Runner");
        System.out.println("-----------------");
        List<Article> lst = articleRepository.findAll();
        for (Article a : lst) {
            System.out.println(a);
        }

        System.out.println("-----------------");
        articleRepository.findByPrix(650.0).forEach(a -> System.out.println(a));

        System.out.println("-----------------");
        articleRepository.findByPrixLessThan(50.0).forEach(System.out::println);

        System.out.println("-----------------");
        articleRepository.findByPrixBetween(50.0, 100.0).forEach(System.out::println);

        System.out.println("-----------------");
        articleRepository.findByDescriptionLike("m__%").forEach(System.out::println);

        System.out.println("-----------------");
        articleRepository.findByPrixLessThanOrderByPrixDescDescriptionAsc(500.0).forEach(System.out::println);

        System.out.println("-----------------");
        articleRepository.findByMarqueNom("Marque A").forEach(System.out::println);

        System.out.println("-----------------");
        articleRepository.findTop3ByOrderByPrixDesc().forEach(System.out::println);

        // JPQL
        System.out.println("-----------------");
        articleRepository.findByPrixLessThanJPQL(100.0).forEach(System.out::println);

        System.out.println("-----------------");
        articleRepository.findByPrixLessThanJPQL2(100.0).forEach(System.out::println);

        System.out.println("-----------------");
        articleRepository.findByPrixLessThanJPQL3(200.0, Emballage.CARTON).forEach(System.out::println);

        System.out.println("-----------------");
        articleRepository.findByNomMarque("Marque B").forEach(System.out::println);

        System.out.println("-----------------");
        List<Article> lstAr = articleRepository.findByNomFournisseur("Fournisseur 1");
        lstAr.forEach(System.out::println);
        System.out.println(lstAr.get(0).getFournisseurs());

        System.out.println("-----------------");
        articleRepository.prixInferieurA(40.0).forEach(System.out::println);

        System.out.println("-----------------");
        System.out.println(articleRepository.removeById(3000L));

        System.out.println("-----------------");
        System.out.println(articleRepository.existsByEmballage(Emballage.PAPIER));

        System.out.println("-----------------");
        System.out.println(articleRepository.existsByEmballage(Emballage.CARTON));

        System.out.println("-----------------");
        System.out.println(articleRepository.countByPrixGreaterThan(100.0));

        System.out.println("-----------------");
        Optional<Marque> opt = marqueRepository.findById(2L);
        Marque mb = opt.get();
        articleRepository.deleteByMarque(mb);

        System.out.println("-----------------");
        Marque mf = new Marque();
        mf.setDateCreation(LocalDate.of(1986, 1, 10));
        mf.setNom("Marque F");
        System.out.println(marqueRepository.saveAndFlush(mf));

        System.out.println("-----------------");
        Article a = new Article();
        a.setDescription("tv OLED");
        a.setPrix(400.0);
        a.setEmballage(Emballage.CARTON);

        a.setMarque(mf);
        mf.getArticles().add(a);
        articleRepository.saveAndFlush(a);
        System.out.println("-----------------");
        
        // Pagination
        articleRepository.findAllBy(Pageable.unpaged()).forEach(System.out::println);

        System.out.println("-----------------");
        Pageable pageable = PageRequest.of(3, 4);
        articleRepository.findAllBy(pageable).forEach(System.out::println);

        System.out.println("-----------------");
        Page<Article> page = articleRepository.findAll(pageable);
        System.out.println(page.getNumberOfElements());
        System.out.println(page.getNumber());
        System.out.println(page.getSize());
        System.out.println(page.getTotalElements());
        System.out.println(page.getTotalPages());

        page.getContent().forEach(System.out::println);

        System.out.println("-----------------");
        Sort sort = Sort.by(Sort.Order.by("prix"));
        pageable = PageRequest.of(0, 4, sort);
        articleRepository.findAllBy(pageable).forEach(System.out::println);

        // Procédure Stockée
        System.out.println("-----------------");
        System.out.println(articleRepository.countInfMontant(20.0));
        System.out.println("-----------------");
        System.out.println(articleRepository.get_count_by_prix2(20.0));
        System.out.println("-----------------");
    //    System.out.println(articleRepository.countInfMontantSQL(20.0));
        
        // Custom repository
        marqueCustomRepository.findByNom("Marque A", null).forEach(System.out::println);
        marqueCustomRepository.findByNom(null, LocalDate.of(1986, 1, 10)).forEach(System.out::println);
        
        // EntityGraph
        List<Marque> marques=marqueRepository.findByNomLike("Marque%");
        marques.forEach(System.out::println);
        System.out.println(marques.get(0).getArticles());
    }
}
