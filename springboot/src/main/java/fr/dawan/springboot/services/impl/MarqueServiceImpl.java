package fr.dawan.springboot.services.impl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.entities.relation.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.MarqueService;

@Service
@Transactional
public class MarqueServiceImpl implements MarqueService {

    @Autowired
    private MarqueRepository marqueRepository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<MarqueDto> getAllMarques(Pageable page) {

//      List<MarqueDto> lstDto=new ArrayList<>();
//      for(Marque m: lstMarque) {
//          MarqueDto dto=mapper.map(m, MarqueDto.class);
//          lstDto.add(dto);
//      }
//      return lstDto;

        List<Marque> lst = marqueRepository.findAll(page).getContent();
        List<MarqueDto> lstDto = lst.stream().map(m -> mapper.map(m, MarqueDto.class)).toList(); // en java 16 +, collect(Collectors.toList());                                                                               // en java 8 à 15
        return lstDto;
    }

    @Override
    public MarqueDto getMarqueById(long id) {
        Optional<Marque> m = marqueRepository.findById(id);
        return mapper.map(m.get(), MarqueDto.class); // entité -> dto
    }

    @Override
    public List<MarqueDto> getMarqueByNom(String nom) {
        return marqueRepository.findByNomLike(nom).stream().map(m -> mapper.map(m,MarqueDto.class)).toList();
    }

    @Override
    public boolean deleteMarque(long id) {
        return marqueRepository.removeById(id) != 0;
    }

    @Override
    public MarqueDto saveOrUpdate(MarqueDto marqueDto) {
        Marque m = mapper.map(marqueDto, Marque.class); // dto -> entité
        Marque mr = marqueRepository.saveAndFlush(m);
        return mapper.map(mr, MarqueDto.class);         // entité -> dto
    }

}
