package fr.dawan.springboot.repositories;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.dawan.springboot.entities.relation.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

@Repository
public class MarqueCustomRepositoryImpl implements MarqueCustomRepository {

    @PersistenceContext
    EntityManager em;

    @Override
    public List<Marque> findByNom(String nom, LocalDate dateCreation) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Marque> cq = cb.createQuery(Marque.class);

        Root<Marque> marque = cq.from(Marque.class);
        cq.where(cb.equal(marque.get("nom"), nom));

        List<Predicate> predicates = new ArrayList<>();

        if (nom != null) {
            predicates.add(cb.equal(marque.get("nom"), nom));
        }
        if (dateCreation != null) {
            predicates.add(cb.equal(marque.get("dateCreation"), dateCreation));
        }
        cq.where(predicates.toArray(new Predicate[0]));
        return em.createQuery(cq).getResultList();
    }

}
