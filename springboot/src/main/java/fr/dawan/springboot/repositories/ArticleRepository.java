package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.entities.relation.Article;
import fr.dawan.springboot.entities.relation.Emballage;
import fr.dawan.springboot.entities.relation.Marque;

// @Repository // -> implicite
@Transactional
public interface ArticleRepository extends JpaRepository<Article, Long> {

    List<Article> findByPrix(double prix);
    
    List<Article> findByPrixLessThan(double prixMax);
    
    List<Article> findByPrixGreaterThanAndEmballage(double prixMin,Emballage emballage);
    
    List<Article> findByPrixBetween(double prixMin, double prixMax);
    
    List<Article> findByDescriptionLike(String model);

    List<Article> findByPrixLessThanOrderByPrixDescDescriptionAsc(double prixMax);

    // IgnoreCase
    List<Article> findByMarqueNomIgnoreCase(String marque);
    
    // Expression de chemin ->uniquement avec les @OneToOne et les @ManyToOne
    List<Article> findByMarqueNom(String nomMarque);
    
    // Top ou First -> en SQL LIMIT
    List<Article> findTop3ByOrderByPrixDesc();
    
    Article findTopByOrderByPrix(); // 1 seul objet
    
    // sujet exist
    boolean existsByEmballage(Emballage emb);
    
    // sujet count
    int countByPrixGreaterThan(double prixMin);
    
    // void ou int pour le type retour
    int deleteByMarque(Marque m);
    
    int removeById(long id);
    
    // JPQL
    
    @Query("SELECT a FROM Article a WHERE a.prix<:prix")
    List<Article> findByPrixLessThanJPQL(@Param("prix")double prixMax);
    
    @Query("FROM Article a WHERE a.prix<:prixMax")
    List<Article> findByPrixLessThanJPQL2(double prixMax);
    
    // paramètre de position -> ?
    @Query("SELECT a FROM Article a WHERE a.prix<?1 AND a.emballage=?2")
    List<Article> findByPrixLessThanJPQL3(double prixMax,Emballage emb);
    
    // Expression de chemin ->uniquement avec les @OneToOne et les @ManyToOne
    @Query("FROM Article a WHERE a.marque.nom=:nomMarque")
    List<Article> findByNomMarque(String nomMarque);
    
    // Jointure Interne
    @Query("FROM Article a JOIN FETCH a.fournisseurs f WHERE f.nom=:nomFournisseur")
    List<Article> findByNomFournisseur(String nomFournisseur);
    
    // SQL
    @Query(nativeQuery = true,value="SELECT * FROM articles WHERE prix<:prix")
    List<Article> prixInferieurA(double prix);
    
    // Pagination
    List<Article> findAllBy(Pageable page);
    
    // Procédure Stockée
    // Appel Explicite
    @Procedure("GET_COUNT_BY_PRIX2")
    int countInfMontant(double prix);
    
    // Appel implicite
    @Procedure
    int get_count_by_prix2(@Param("montant") double prix);
    
    // SQL
    @Query(nativeQuery = true,value="CALL GET_COUNT_BY_PRIX(:prix)")
    int countInfMontantSQL(@Param("prix")double prix);

}
