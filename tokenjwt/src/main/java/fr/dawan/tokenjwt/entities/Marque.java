package fr.dawan.tokenjwt.entities;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Lob;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "marque")
public class Marque extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(length = 40, nullable = true)
    private String nom;

    @Column(name = "date_creation", nullable = true)
    private LocalDate dateCreation;

    @Lob
    @Column(length = 65000)
    private byte[] logo;

    @OneToMany(mappedBy = "marque")
    @Exclude
    private Set<Article> articles = new HashSet<>();

    public Marque(String nom, LocalDate dateCreation) {
        this.nom = nom;
        this.dateCreation = dateCreation;
    }
}
