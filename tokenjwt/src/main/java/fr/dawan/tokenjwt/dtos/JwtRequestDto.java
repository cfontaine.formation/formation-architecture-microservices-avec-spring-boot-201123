package fr.dawan.tokenjwt.dtos;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class JwtRequestDto {

    @NotNull
    @Size(max=60)
    private String username;

    @NotNull
    @Size(max=80)
    private String password;

}
