package fr.dawan.tokenjwt.dtos;

import java.time.LocalDate;

import fr.dawan.tokenjwt.enums.Emballage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ArticleDto {

    private long id;
    
    private String description;
    
    private double prix;
    
    private Emballage emballage;
    
    private long marqueId;
    
    private String marqueNom;
    
    private LocalDate marqueDateCreation;

}
