package fr.dawan.tokenjwt.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import fr.dawan.tokenjwt.dtos.ArticleDto;
import fr.dawan.tokenjwt.dtos.ArticleDtoPost;

public interface ArticleService {
    
    List<ArticleDto> getAll(Pageable page);
    
    Optional<ArticleDto> getById(long id);
    
    List<ArticleDto> getByDescription(String nom);
    
    List<ArticleDto> getByMarqueNom(String nomMarque,Pageable page);
    
    boolean deleteById(long id);
    
    ArticleDto save(ArticleDtoPost articleDto);
    
    Optional<ArticleDto> update(long id,ArticleDtoPost articleDto);

}
