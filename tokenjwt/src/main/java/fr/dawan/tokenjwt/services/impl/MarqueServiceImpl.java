package fr.dawan.tokenjwt.services.impl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.tokenjwt.dtos.MarqueDto;
import fr.dawan.tokenjwt.entities.Marque;
import fr.dawan.tokenjwt.repositories.MarqueRepository;
import fr.dawan.tokenjwt.services.MarqueService;

@Service
@Transactional(readOnly = true)
public class MarqueServiceImpl implements MarqueService {
    
    @Autowired
    private MarqueRepository repository;
    
    @Autowired
    private ModelMapper mapper;

    @Override
    public List<MarqueDto> getAll(Pageable page) {
        return repository.findAll(page).stream().map(a -> mapper.map(a, MarqueDto.class)).toList();
    }

    @Override
    public Optional<MarqueDto> getById(long id) {
        return repository.findById(id).map(a -> Optional.of(mapper.map(a, MarqueDto.class)))
                .orElse(Optional.empty());   
    }

    @Transactional
    public int deleteById(long id) {
        return repository.removeById(id);
    }

    @Transactional
    @Override
    public MarqueDto save(MarqueDto marqueDto) {
        return mapper.map(repository.saveAndFlush(mapper.map(marqueDto, Marque.class)), MarqueDto.class);
    }

    @Transactional
    @Override
    public MarqueDto update(MarqueDto marqueDto) {
        // TODO Auto-generated method stub
        return null;
    }

}
