package fr.dawan.tokenjwt.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import fr.dawan.tokenjwt.dtos.MarqueDto;

public interface MarqueService {
    
    List<MarqueDto> getAll(Pageable page);
    
    Optional<MarqueDto> getById(long id);
    
    int deleteById(long id);
    
    MarqueDto save(MarqueDto articleDto);
    
    MarqueDto update(MarqueDto articleDto);
}
