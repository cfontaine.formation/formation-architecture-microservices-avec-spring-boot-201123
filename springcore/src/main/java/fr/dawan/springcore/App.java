package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.DtoMapper;
import fr.dawan.springcore.components.ArticleRepository;
import fr.dawan.springcore.components.ArticleService;

public class App {
    public static void main(String[] args) {
        // Exemple lombok
//       Article a=new Article("tv",350.0);
//        System.out.println(a.getPrix());
//       System.out.println(a);
//       
//       // Builder
//       Article a1=Article.builder().description("Stylo").prix(3.5).build();
//       System.out.println(a1);

        // Spring Core
        // Création du conteneur d'ioc
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConf.class);

        // getBean -> permet de récupérer les instances des beans depuis le conteneur
        DtoMapper m1 = ctx.getBean("mapper1", DtoMapper.class);
        System.out.println(m1);

        ArticleRepository r1 = ctx.getBean("repository1", ArticleRepository.class);
        System.out.println(r1);

        ArticleRepository r12 = ctx.getBean("repository1", ArticleRepository.class);
        System.out.println(r12);

        ArticleService srv1 = ctx.getBean("service1", ArticleService.class);
        System.out.println(srv1);

        // Fermeture du context entraine la destruction de tous les beans
        ((AbstractApplicationContext) ctx).close();
    }
}
