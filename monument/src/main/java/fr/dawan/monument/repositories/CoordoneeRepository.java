package fr.dawan.monument.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.monument.entities.Coordonne;

public interface CoordoneeRepository extends JpaRepository<Coordonne, Long> {

}
