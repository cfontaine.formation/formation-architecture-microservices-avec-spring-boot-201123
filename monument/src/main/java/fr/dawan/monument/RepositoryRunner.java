package fr.dawan.monument;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.monument.repositories.EtiquetteRepository;
import fr.dawan.monument.repositories.MonumentRepository;

@Component
public class RepositoryRunner implements CommandLineRunner {

    @Autowired
    MonumentRepository monumentRepository;
    
    @Autowired
    EtiquetteRepository etiquetteRepository;

    @Override
    public void run(String... args) throws Exception {
        monumentRepository.findByAnneeConstructionBetweenOrderByAnneeConstructionDesc(2000, 2023).forEach(System.out::println);
        System.out.println("_________________________________________");
        
        monumentRepository.findByNomLike("T__%").forEach(System.out::println);
        System.out.println("_________________________________________");
        System.out.println(monumentRepository.findByCoordonneLatitudeAndCoordonneLongitude(41.8905556, 12.4925));
        System.out.println("_________________________________________");
        
        monumentRepository.findByLocalisationPaysIn(Pageable.unpaged(),Arrays.asList("France", "Italie")).forEach(System.out::println);
        System.out.println("_________________________________________");
        
        monumentRepository.findTop5ByOrderByAnneeConstruction().forEach(System.out::println);
        System.out.println("_________________________________________");
        
        monumentRepository.findByEtiquetteIntitule("Statue").forEach(System.out::println);
        System.out.println("_________________________________________");
        etiquetteRepository.findByIntituleLike("%expo%").forEach(System.out::println);
    }

}
